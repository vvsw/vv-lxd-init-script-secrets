# vv-lxd-init-scipt-secrets

follow the rules

rules:
- all files in this repo must be ansible-vault encrypted 

- root folder holds all individual directories per lxd-container-name

- within the individual container folder, one yml file per variable

ie: 
repo_parent_dir/lxd_container_x/$VARIABLE_NAME

contents of VARIABLE_NAME is VARIABLE_VALUE

ie:
SECRET_VAR_NAME=TEST_SECRET_X
SECRET_VAR_VALUE=PASSWORD123
LXD_CONTAINER_NAME=test-container-0

$ echo $SECRET_VAR_NAME:$SECRET_VAR_VALUE > vv_lxd_init_script_secrets/$LXD_CONTAINER_NAME/$SECRET_VAR_VALUE
$ ansible-vault encrypt vv_lxd_init_script_secrets/$LXD_CONTAINER_NAME/$SECRET_VAR_VALUE


if you dont like these documents, change it. 

lxd-containers-** and vv-box-** separation to be more separated.
